#!/usr/bin/env node
const fs = require("fs");
const fetch = require("node-fetch");
const path = require("path");

const WORLDS_URL = "https://api.tibiadata.com/v4/worlds";
const OUTPUT_PATH = path.join(__dirname, "./src/@globals/worlds.js");

const TEMPLATE = (list) => {
  return `const WORLDS = ${list}

module.exports = {
  WORLDS
}`
}

function createFile(filePath, data, callback) {
  fs.writeFile(filePath, data, (err) => {
    if (err) {
      throw err;
    }
    callback();
  })
}

function fetchWorlds(callback) {
  fetch(WORLDS_URL, { method: "get", headers: { "Content-Type": "application/json" } })
  .then((res) => res.json())
  .then((json) => callback(json))
  .catch((error) => {
    console.log("[ERROR: Failed to retrieve world list.]");
  })
}

function init() {
  fetchWorlds((response) => {
    const WORLDS = response.worlds.regular_worlds.reduce((final, current) => {
      const name = current.name;
      return {
        ...final,
        [name]: name,
      };
    }, {});
    createFile(OUTPUT_PATH, TEMPLATE(JSON.stringify(WORLDS, null, 2)), () => {
      console.log("all good");
    })
  });
}

init();
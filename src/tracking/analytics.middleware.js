import { gtag } from "./gtag-functions";

// import action names here?

// do not track these
const EXCLUDED_ACTIONS = ["SET_META", "SET_ME"];

const trackingInformation: any = {
    // gets tracking information just by filtering the action
    "SAMPLE_ONE": ({id, content}) => ({id, length: content.length}),
    // gets tracking information from the action  and the store
    "SAMPLE_TWO": ({id, enabled}, store) => ({
        id, friends: store.getState().friends
    })
};

const massagePayload = (payload: any) => {
    return Object.entries(payload).map((item: any, i: number) => {
        if (item[1] && item[1].constructor === Array) {
            return [item[0], {
                type: "ARRAY",
                length: item[1].length
            }];
        }
        return item;
    });
}


const analyticsMiddleware = store => next => action => {
    const {type, ...payload } = action;
    if (!EXCLUDED_ACTIONS.includes(action.type)) {
        gtag.action({
            event_category: action.type,
            // defaults to tracking the entire stringified payload
            event_label: Object.keys(payload),
            value: JSON.stringify(trackingInformation[action.type]
                ? trackingInformation[action.type](payload, store)
                : massagePayload(payload) || {})
        });
    }
    if (action.type === "SET_ME") {
        gtag.set(
            {
                world: payload.world,
                ...payload.player
            }
        );
    }
    return next(action);
};
export default analyticsMiddleware;
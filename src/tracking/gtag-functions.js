export const gtag: any = {
    "action": (payload: any) => {
        if (window.gtag) {
            // console.log(payload);
            // eslint-disable-next-line no-undef
            window.gtag("event", "action", payload);
        }
    },
    "set": (payload: any) => {
        if (window.gtag) {
            // console.log(payload, "set");
            window.gtag("set", payload);
        }
    }
}
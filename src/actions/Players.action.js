import WorldService from "../services/world.service";
import { whenToInvalidate } from "../@globals/APICacheTimes.js";

export const setPlayers = (world: World | string, players: Player[], players_online: number, last_updated: string | Date | moment.Date) => {
  return {
    type: "SET_PLAYERS",
    world,
    players,
    players_online,
    last_updated
  }
}; //RECEIVE_POSTS

const REQUEST_PLAYERS = 'REQUEST_PLAYERS';
function requestPlayers(world: World) {
  return {
    type: REQUEST_PLAYERS,
    world
  }
}

const INVALIDATE_PLAYERS = 'INVALIDATE_PLAYERS';
export function invalidatePlayers(world: World) {
  return {
    type: INVALIDATE_PLAYERS,
    world
  }
}

let timers = {};

export function fetchPlayers(world: World) {
  return function(dispatch) {
    dispatch(requestPlayers(world));
    return WorldService.get(world)
      .then(
        (response) => {
          const w = response.world;
          if (!w.players_online) {
            dispatch(invalidatePlayers(world));
            console.log("API fail, no players returned.");
            return Promise.resolve();
          }
          const now = new Date().toISOString();
          const lastUpdated = response.information.timestamp;
          dispatch(setPlayers(world, w.online_players, w.players_online, lastUpdated));
          clearTimeout(timers[world]);
          timers[world] = setTimeout(() => {
            dispatch(invalidatePlayers(world));
          }, whenToInvalidate(now, lastUpdated, "Worlds"));
        },
        (error) => console.log("An error occurred.", error)
      );
  }
}

function shouldFetchPlayers(state: any, world: World): boolean {
  //? globalReducer
  const playersByWorld = state.globalReducer.playersByWorld[world];
  if (!playersByWorld || (playersByWorld.players || []).length === 0) {
    return true;
  } else if (playersByWorld.isFetching) {
    return false;
  } else {
    return playersByWorld.didInvalidate;
  }
}

export function fetchPlayersIfNeeded(world: World) {

  return (dispatch, getState) => {
    if (shouldFetchPlayers(getState(), world)) {
      // Dispatch a thunk from thunk!
      // console.log(`[Note] Players loaded for: (${world})`);
      return dispatch(fetchPlayers(world))
    } else {
      // Let the calling code know there's nothing to wait for.
      // console.log(`[Note] Players from cache for: (${world})`);
      return Promise.resolve()
    }
  }
}

export const setVisibilityFilters = (world: World, filter: any) => ({
  type: "SET_VISIBILITY_FILTERS",
  world,
  filter
});

export const resetFilters = (world: World) => ({
  type: "SET_VISIBILITY_FILTERS",
  world,
  filter: {type: "SHOW_ALL"}
});

export const addVisibilityFilters = (world: World, filter: any) => ({
  type: "ADD_VISIBILITY_FILTERS",
  world,
  filter
});

export const setMe = (world: World, player: Player) => ({
  type: "SET_ME",
  world,
  player
});

export const resetOrGetMe = (world: World) => ({
  type: "RESET_OR_GET_ME",
  world
});



// export const VisibilityFilters = {
//   SHOW_ALL: 'SHOW_ALL',
//   SHOW_COMPLETED: 'SHOW_COMPLETED',
//   SHOW_ACTIVE: 'SHOW_ACTIVE'
// }
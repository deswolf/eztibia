export const setMeta = (meta: any) => {
  return {
    type: "SET_META",
    meta
  }
};
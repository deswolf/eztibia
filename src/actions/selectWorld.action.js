import { resetOrGetMe } from "./Players.action";

export const setWorld = (world: World) => {
  return dispatch => {
    dispatch({
      type: "SET_WORLD",
      world
    })
    dispatch(resetOrGetMe(world))
  }
  // return {
  //   type: "SET_WORLD",
  //   world
  // }
};
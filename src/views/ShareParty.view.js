// @flow
import React, { Component } from 'react';
import update from 'immutability-helper';

// import WorldService from "services/world.service";

import Moment from 'react-moment';
// import moment from "moment";
import { Tooltip } from 'react-tippy';
import { Menu, Item, MenuProvider } from 'react-contexify';
import 'react-contexify/dist/ReactContexify.min.css';
// import { toast } from 'react-toastify';

import Loader from "../components/Loader.component";
// import PlayerLoader, { LOADING_PLAYER_ARRAY } from "components/empties/PlayerLoader.component";
import InlineError from "../components/InlineError.component";
import WorldNotSelected from "../components/WorldNotSelected.component";

import { classes, copyToClipboard, openInNewTab, inputChange, isPlayerInLevelBracket, truncate, worldExists } from "../helpers";
// import { WORLDS } from "@globals/worlds";
import { VOCATIONS, VOCATION_ICONS, VOCATION_NAMES_FULL, vocTransformer } from "../@globals/vocations";
import { DEFAULT_ME, BONUS_EXP_PERCENTAGE_DIFFERENT_VOCATIONS } from "../@globals/defaults";
// import { differenceBy } from 'lodash';

import { connect } from "react-redux";
import {
  // setPlayers,
  setVisibilityFilters,
  setMe,
  fetchPlayersIfNeeded,
  //invalidatePlayers
  // resetFilters,
  // addVisibilityFilters
} from "../actions/Players.action";


type State = {
  // loading: boolean,
  // refreshing: boolean,
  // lastRefreshed: Date | Moment.Date;
  //world: string,
  // players_online: number,
  players_available: number,
  players_matching: any,
  // players: Player[],
  level: number,
  // vocations: Vocation | null,
  selectedDifferentVocationCount: number,
  chosenPlayers: Player[],
  minLevel: number,
  maxLevel: number,
  initMinLevel: number,
  initMaxLevel: number,
  // me: Player | null,
  possibleMes: Player[],
  initialState: any
};

type Props = {
  //world: any;
  match: any;
  history: any;
  location: any;
  onFocusWorldSelect: Function;
  globalReducer: any;
  setPlayers: Function;
  setVisibilityFilters: Function;
  setMe: Function;
  fetchPlayersIfNeeded: Function;
  //invalidatePlayers: Function;
}

// let worldChangedRecently: boolean = false;

// const rcmOnClick = ({ event, props }) => console.log(event, props);
const rcmCopy = ({ event, props }) => copyToClipboard(props.player.name);
// *{props.player.name}* hi
const rcmOpenSite = ({ event, props }) => openInNewTab(props.site + props.player.name.split(" ").join("+") + "#characters");


const RcMenu = () => (
  <Menu id="menu_rcm">
    <Item onClick={rcmCopy}>Copy Name</Item>
    <Item onClick={rcmOpenSite} data={{ site: "https://www.tibia.com/community/?subtopic=characters&name=" }}>View player on Tibia.com</Item>
    <Item onClick={rcmOpenSite} data={{ site: "http://www.tibiaring.com/char.php?c=" }}>View player on TibiaRing.com</Item>
  </Menu>
);

const TRUNCATE_NAME_LENGTH = 13;

const GENERATE_UNKNOWN_PLAYER = (level: number, full_vocation: any): any => {
  return {
    full_vocation: full_vocation,
    level: level,
    name: "?",
    vocation: full_vocation.split(" ")[1]
  };
};

const VOCATION_COUNT: { [key: string]: number } = {
  ...vocTransformer<number>(VOCATION_NAMES_FULL, () => 0),
  None: 0
};

class SharePartyView extends Component<Props, State> {

  state = {
    // loading: false,//true,
    // refreshing: false,
    // lastRefreshed: new Date(),
    //world: this.props.world,
    // players_online: 0,
    players_available: 0,
    players_matching: VOCATION_COUNT,
    // players: [],
    // level: 0,
    // vocations: null,
    chosenPlayers: [],
    selectedDifferentVocationCount: 0,
    minLevel: 0,
    maxLevel: 0,
    initMinLevel: 0,
    initMaxLevel: 0,
    // me: null,
    possibleMes: [],
    initialState: {}
  };

  levelInputRef: any = React.createRef();

  get world() {
    return this.props.globalReducer.world;
  }

  get worldExists() {
    return worldExists(this.props.globalReducer.world);
  }

  get playersByWorld() {
    return this.props.globalReducer.playersByWorld[this.props.globalReducer.world] || null;
  }

  get isFetching() {
    if (!this.playersByWorld || this.playersByWorld.isFetching) {
      return true;
    }
    return false;
  }

  // toastId = null;
  // dismiss = () =>  toast.dismiss(this.toastId);

  // notifyLogged = (loggedIn: number, loggedOut: number, world: World, last_updated: Date): void => toast.info(
  //   <div>
  //     <h3>Number of players in {world}</h3>
  //       <p>logged in <span>{loggedIn}</span></p>
  //       <p>logged out <span>{loggedOut}</span></p>
  //       <p className="footer-date-notification">since <Moment interval={10} fromNow>{last_updated}</Moment></p>
  //   </div>, {autoClose:9000}
  // );



  resetState(): void {
    this.setState({ ...this.state.initialState });
  }

  getShareLevel(type: "min" | "max", level: number): number {
    return {
      "min": () => Math.round(level / 3 * 2), //floor?
      "max": () => Math.round(level / 2 * 3)  //ceil?
    }[type]();
  }

  getDifferentVocationCount(me: Player | null, players: Player[]): number {
    if ((players.length === 0) || (players.length === 1 && !me)) {
      return 0;
    }
    let count = {...VOCATION_COUNT};
    players.forEach((player) => {
      count[player.vocation] += 1;
    });
    if (me) {
      count[me.vocation] += 1;
    }
    //TODO: None vocation shouldn't be taken into account
    return Object.entries(count).reduce((acc, [key, value]) => value >= 1 && key !== "None" ? acc += 1 : acc, 0);
  }

  updateMinMaxLevels(levels: number[]): void {
    levels.sort((a, b) => { return a - b });
    let min: number = this.getShareLevel("min", levels[levels.length - 1]);
    let max: number = this.getShareLevel("max", levels[0]);
    //TODO: x
    this.setState({
      minLevel: min,
      maxLevel: max,
      players_matching: this.playerCount(this.playersByWorld.players, min, max)
    });
    
    const firstTime = levels.length === 1 ? true : false;
    if (firstTime) {
      this.props.setVisibilityFilters(this.world, [
        {type: "LEVEL_FILTER", min: min, max: max}
      ]);  
      this.setState({
        initMinLevel: this.getShareLevel("min", (this.playersByWorld || {me: DEFAULT_ME}).me.level),
        initMaxLevel: this.getShareLevel("max", (this.playersByWorld || {me: DEFAULT_ME}).me.level)
      });
    }
    // this.props.setVisibilityFilters([
    //   {type: "LEVEL_FILTER", min: 150, max: Infinity},
    //   {type: "VOCATION_FILTER", vocation: "Paladin"},
    //   {type: "PROMOTED_FILTER"}
    // ]);
    // console.log(this.props.globalReducer.filteredPlayers);
  }

  playerCount(players: Player[], min: number, max: number): $TODO {
    let count = {...VOCATION_COUNT};
    players.forEach(player => {
      if (isPlayerInLevelBracket(min, max, player.level)) {
        count[player.vocation] += 1;
      }
    });
    const players_matching = update(this.state.players_matching, 
      {$set: count}
    );
    this.setState({players_available: Object.values(count).reduce((acc, v) => acc + v, 0) });
    return players_matching;
  }


  componentDidMount() {
    let { initialState, level, ...is } = this.state;
    this.setState({ initialState: is })
    this.doInitStuff();
    // console.log(this.state.vocations);
  }

  doInitStuff() {
    if (this.worldExists) {
      // this.setState({ loading: true });
      // this.props.invalidatePlayers(this.world);
      this.loadPlayers(this.world);
      this.levelInputRef.select();
    }
  }

  componentDidUpdate(prevProps: any): void {
    if (prevProps.globalReducer.world !== this.props.globalReducer.world) {
    //   worldChangedRecently = true;
    // }
    // if (worldChangedRecently && !this.isFetching) {
    //   worldChangedRecently = false;
      this.resetState();
      this.loadPlayers(this.props.globalReducer.world);
      (this.playersByWorld || {me: DEFAULT_ME}).me.level && this.levelInputRef.select();
    }
  }


  loadPlayers(world: World): void {
    // const timeLoaded: any = Object.assign({}, {first: !this.props.globalReducer.playersByWorld[world]});

    this.props.fetchPlayersIfNeeded(world).then(() => {
      // if (!timeLoaded.first) {
      //   this.notifyLogged(this.playersByWorld.loggedIn, this.playersByWorld.loggedOut, world, this.props.globalReducer.playersByWorld[world].last_updated);
      // }
      const meLevel = (this.playersByWorld || {me: DEFAULT_ME}).me.level;
      const levelList = this.getLevelList(meLevel, this.state.chosenPlayers);
      this.setState({ possibleMes: this.getPossibleMes(meLevel) });
      this.updateMinMaxLevels(levelList);
      // console.log(this.props.globalReducer);
    });
  }


  inputCb(data: any): void {
    const {name, value} = data;
    // console.log(name, value);
    this.setState({ [name]: value }, () => {
      this.inputHandler(name, value);
    });
  }

  updateLevel(data: any): void {
    const { value } = data;
    const level: number = value === "" || value < 0 ? 0 : value;
    this.levelChange([level]);
    this.setState({ possibleMes: this.getPossibleMes(level) });
    return;
  }

  inputHandler(name: string, value: any): void {
    // if (name === "level") {
    //   const level: number = value === "" || value < 0 ? 0 : value;
    //   this.levelChange([level]);
    //   this.setState({ possibleMes: this.getPossibleMes(level) });
    //   return;
    // }
    if (name === "vocations") {
      // this.setState({ players: this.filterPlayers(this.state.players, this.state.minLevel, this.state.maxLevel) })
      return;
    }
  }

  levelChange(levels: number[]): void {
    this.setState({ chosenPlayers: [] });
    // this.setMe(null);
    this.setMe(this.world, {level: levels[0]});
    this.updateMinMaxLevels(levels);
  }

  setMe(world: World, player: Player | null): void {
    this.setState({
      selectedDifferentVocationCount: this.getDifferentVocationCount(player, this.state.chosenPlayers)
    });
    this.props.setMe(world, player);
  }

  getPossibleMes(level: number): Player[] {
    if (level === 0) {
      return [];
    }
    return this.playersByWorld.players.filter((p) => p.level === level);
  }


  //TODO: x
  selectPlayer($event: $TODO, player:Player, isMe: boolean): boolean {
    if (isMe) {
      return false;
    }
    //TODO: immutibality helper
    const selectedPlayers: Player[] = JSON.parse(JSON.stringify(this.state.chosenPlayers)); // clone first
    // const player: Player = this.state.filteredPlayers[key][index];
    const i: number = selectedPlayers.findIndex((p) => p.name === player.name);
    console.log(i);
    if (i !== -1) {
      selectedPlayers.splice(i, 1);
    } else {
      selectedPlayers.push(player);
    }
    //
    let levels: number[] = this.getLevelList((this.playersByWorld || {me: DEFAULT_ME}).me.level, selectedPlayers);
    this.updateMinMaxLevels(levels);
    this.setState({ 
      chosenPlayers: selectedPlayers,
      selectedDifferentVocationCount: this.getDifferentVocationCount(this.playersByWorld.me, selectedPlayers)
    });
    return true;
    // console.log(selectedPlayers);
    // console.log(this.state.chosenPlayers);
    // console.log(index, key);
  }

  getLevelList(level: number, selectedPlayers: Player[]): number[] {
    let levels: number[] = [level];
    selectedPlayers.forEach((player) => {
      levels.push(player.level);
    });
    return levels;
  }

  showVocation(vocation: Vocation | "all", e: MouseEvent): void {
    e.preventDefault();
    let filters = [this.playersByWorld.filters.find((filter) => filter.type === "LEVEL_FILTER")];
    if (vocation !== "all") {
      filters = [
        ...filters,
        {type: "VOCATION_FILTER", vocation: vocation}
      ];
    }
    this.props.setVisibilityFilters(this.world, filters);
  }

  renderPlayerNew(player: Player, key: Vocation | string, index: number, vocationToRender: any): $TODO {
    const selected: boolean = ~this.state.chosenPlayers.findIndex((p) => p.name === player.name) ? true : false;
    const isMe: boolean = (this.playersByWorld.me || {}).name === player.name ? true : false;
    // const isInactive: boolean = !this.isPlayerVisible(this.state.initMinLevel, this.state.initMaxLevel, player.level);
    const isHidden: boolean = this.state.chosenPlayers.length >= 1 ? !isPlayerInLevelBracket(this.state.minLevel, this.state.maxLevel, player.level) : false;
    // const vocationToRender: string = this.state.vocations.length === 1 && this.state.vocations[0] ? this.state.vocations[0] : "all";

    return (vocationToRender.vocation === null || vocationToRender.vocation === player.vocation) && <div key={player.name} className="player-container">
      <MenuProvider className={classes({'player-box box': true, 'is-selected': selected, 'is-selected-me': isMe, 'is-inactive': isHidden })} onClick={($event) => !isHidden && this.selectPlayer($event, player, isMe)} id="menu_rcm" component="div" data={{ player: player }}>
        <article className="media">
          <div className="media-left">
          <Tooltip title={player.full_vocation} position="bottom" disabled={isHidden}>
            <figure className="image is-32x32">
              <img src={VOCATION_ICONS[player.vocation]} alt={player.vocation}/>
            </figure>
          </Tooltip>
          </div>
          <div className="media-content">
            <div className="content">
                <div className="player-title"><strong>{truncate(player.name, TRUNCATE_NAME_LENGTH)}</strong></div>
                <div className="player-meta">{player.level}</div>
            </div>
          </div>
          <div className="media-right">
            {(this.playersByWorld || {me: DEFAULT_ME}).me.level === player.level && !this.props.globalReducer.me && <i className="far fa-meh-blank pointer"></i>} {!isMe && <Tooltip title={selected ? "Remove from party" : "Add to party"} position="bottom" disabled={isHidden}><i className={selected ? "fas fa-user-minus pointer" : "fas fa-user-plus pointer"}></i></Tooltip>}
          </div>
        </article>
      </MenuProvider>
      </div>;
  }

  refresh = () => {
    // this.setState({ refreshing: true });
    // this.props.invalidatePlayers(this.world);
    !this.isFetching && this.loadPlayers(this.world);
  }


  render() {
    if (!this.worldExists) {
      return <WorldNotSelected onFocusWorldSelect={this.props.onFocusWorldSelect} />;
    }
    // const { globalReducer } = this.props;
    // if (!this.playersByWorld || this.playersByWorld.isFetching) {
    //   return <div>fetching</div>;
    // }

    let vocationToRender: any = (this.playersByWorld && this.playersByWorld.filters.find((filter) => filter.type === "VOCATION_FILTER")) || {vocation: null};
    const canRefresh: boolean = (this.playersByWorld || {}).didInvalidate || this.isFetching;
    const meLevel: number = (this.playersByWorld || {me: DEFAULT_ME}).me.level;
    // let vocationToRender: any = {vocation: null};
    return (
       <div id="share-party">
         <header className="box" style={{background:"white", padding: "15px 0", marginBottom: "15px"}}>
          <nav className="level">
            <div className="level-item has-text-centered">
              <div>
                <label htmlFor="levelInput" className={classes({"heading": true, "heading-blue": meLevel <= 0})}>Your Level</label>
                <p className="title">
                  <input id="levelInput" ref={n => this.levelInputRef = n} placeholder="?" className={classes({"input": true, "input-blue": meLevel <= 0})} name="level" type="number" min="0" max="9999" maxLength="4" step="1" value={meLevel} onChange={($event) => inputChange($event, this.updateLevel.bind(this))} />
                </p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Min Level</p>
                <p className="title">{this.state.minLevel || "?"}</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Max Level</p>
                <p className="title">{this.state.maxLevel || "?"}</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Party Bonus EXP</p>
                <p className="title">
                  <span>{BONUS_EXP_PERCENTAGE_DIFFERENT_VOCATIONS[this.state.selectedDifferentVocationCount]}%</span>
                  {this.state.chosenPlayers.length >= 1 && !this.playersByWorld.me.vocation && <span className="refresh-holder" style={{color: "#e64848", opacity: "1", cursor: "help"}}>
                    <Tooltip title="Your character not included! Please choose your vocation or select your character from the list." position="bottom">
                      <span className="icon">
                        <i className="fas fa-exclamation" aria-hidden="true"></i>
                      </span>
                    </Tooltip>
                  </span>}
                </p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Players Available</p>
                <p className="title">{this.state.players_available}</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Players Online</p>
                <span className="title">
                  <span>{(this.playersByWorld || {}).players_online}</span>
                  <span className={classes({"refresh-holder": true, "can-refresh": canRefresh})} onClick={() => canRefresh && this.refresh()}>
                    <Tooltip html={<div><span>Updated: </span><Moment fromNow>{(this.playersByWorld || {}).last_updated}</Moment></div>} position="bottom">
                      <span className="icon">
                        <i className={classes({"fas fa-sync loading-paused": true, "loading-refresh": this.isFetching})} aria-hidden="true"></i>
                      </span>
                    </Tooltip>
                  </span>
                </span>
              </div>
            </div>
          </nav>
          <div style={{marginTop: "15px", padding: "0 15px"}}>
            {!(this.playersByWorld || { me: DEFAULT_ME }).me.vocation && meLevel > 0 && <div className="areyou-container" style={{marginBottom: "6px"}}>
              <span className="question">Are you</span>
              <ul>
                {this.state.possibleMes.length > 0 && this.state.possibleMes.map((player) => 
                  <li key={player.name}>
                    <span onClick={() => this.setMe(this.world, player)} className="tag pointer is-warning">{player.name}</span>
                  </li>
                )}
                {Object.values(VOCATION_NAMES_FULL).map((vocation) => 
                  <li key={vocation}>
                    <span onClick={() => this.setMe(this.world, GENERATE_UNKNOWN_PLAYER(meLevel, vocation))} className="tag pointer is-light has-dark-border uppercase select-none">{(vocation.match(/\b(\w)/g) || "? ?").join("")}</span>
                  </li>
                )}
              </ul>
              <span className="question" style={{marginLeft:"6px"}}>?</span>
            </div>}
            <div className="areyou-container">
              <ul>
                {(this.playersByWorld || { me: DEFAULT_ME }).me.vocation ? <li>
                  <div className="tags has-addons">
                    <span className="tag is-dark select-none">you</span>
                    {this.playersByWorld.me.name !== "?" && <span className="tag is-warning has-dark-border" style={{borderRightWidth: "0"}}>{this.playersByWorld.me.name}</span>}
                    <span className="tag is-light has-dark-border uppercase select-none"  style={{borderLeftWidth: "0"}}>{VOCATIONS[this.playersByWorld.me.vocation]} <button onClick={() => this.setMe(this.world, { level: meLevel })} className="delete is-small"></button></span>
                    {/* <span className="tag is-light has-dark-border">?</span> */}
                  </div>
                </li> : <li><span className="tag is-dark">you</span></li>}
                {this.state.chosenPlayers.map((player) => <li key={player.name}>
                  <div className="tags has-addons">
                    <span className="tag is-dark">{player.name}</span>
                    <span className="tag is-light has-dark-border uppercase select-none">{player.level} {VOCATIONS[player.vocation]} <button onClick={($event) => this.selectPlayer($event, player, false)} className="delete is-small"></button></span>
                  </div>
                </li>)}
              </ul>
            </div>
          </div>
        </header>
        <div className="columns">
        <div className="column is-one-quarter">
          <aside className="menu box is-sticky" style={{background:"white",padding:"12px"}}>
            <ul className="menu-list">
              <li>
                <a href="#!" onClick={(e) => this.showVocation("all", e)} className={classes({"is-active": vocationToRender.vocation === null})}>
                  <span>All Vocations</span><span><span className="tag is-white">{this.state.players_available}</span></span>
                </a>
              </li>
              {Object.keys(VOCATION_NAMES_FULL).map((vocation) => 
                <li key={vocation}>
                  <a href="#!" onClick={(e) => this.showVocation(vocation, e)} className={classes({"is-active": vocationToRender.vocation === vocation})}>
                    <span>{vocation}s</span><span><span className="tag is-white">{this.state.players_matching[vocation]}</span></span>
                  </a>
                </li>
              )}
            </ul>
          </aside>
        </div>
        <div className="column player-list">
          {meLevel < 1 && <div><div onClick={() => this.levelInputRef.select()} className="box pointer no-matching"><InlineError className="middle" title="What is your level?" description="Please enter the level of your character" icon="far fa-keyboard" /></div></div>}
          {!this.isFetching || !(this.playersByWorld || { players: [] }).players || meLevel < 1 ? <div>
            {meLevel >= 1 && this.playersByWorld.filteredPlayers.map((player, index) => this.renderPlayerNew(player, player.name, index, vocationToRender))}
            {meLevel >= 1 && (this.state.players_available === 0 || this.state.players_matching[vocationToRender.vocation] === 0) && <div className="box no-matching"><InlineError className="middle" description={`No matching ${!vocationToRender.vocation ? "player" : vocationToRender.vocation.toLowerCase()}s found`} icon="fas fa-blind" /></div>}
          </div> : <div className="isFetching" style={{ marginTop: "60px" }}><Loader size="xl" /></div>}
        </div>
        </div>
        <RcMenu />
      </div>
    );
  }
}



const mapStateToProps = state => {
  return {
    globalReducer: state.globalReducer
  };
};
const mapDispatchToProps = dispatch => ({
  // setPlayers: (players, players_online, last_updated) => dispatch(setPlayers(players, players_online, last_updated)),
  setVisibilityFilters: (world, filter) => dispatch(setVisibilityFilters(world, filter)),
  setMe: (world, player) => dispatch(setMe(world, player)),
  fetchPlayersIfNeeded: (world) => dispatch(fetchPlayersIfNeeded(world)),
  //invalidatePlayers: (world) => dispatch(invalidatePlayers(world))
  // resetFilters: () => dispatch(resetFilters()),
  // addVisibilityFilters: (filter) => dispatch(addVisibilityFilters(filter))
});

export default connect(mapStateToProps, mapDispatchToProps)(SharePartyView);


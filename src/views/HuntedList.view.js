import React, { useEffect, useState, useRef } from "react";
import { classes } from "../helpers";

import Moment from 'react-moment';
import { Tooltip } from 'react-tippy';
import Loader from "../components/Loader.component";

import { connect } from "react-redux";
import {
  fetchPlayersIfNeeded,
} from "../actions/Players.action";
type Props = {
    //world: any;
    match: any;
    history: any;
    location: any;
    onFocusWorldSelect: Function;
    globalReducer: any;
    // setPlayers: Function;
    // setVisibilityFilters: Function;
    // setMe: Function;
    fetchPlayersIfNeeded: Function;
    //invalidatePlayers: Function;
}

const COLSPAN = 5;

const HuntedListView = (props: Props) => {
  const [huntedList, setHuntedList] = useState(JSON.parse(localStorage.getItem("hunted-list-temp")) || ["aalda"]);
  const [onlineList, setOnlineList] = useState([]);
  const isInitialMount = useRef(true);
  // const world = props.globalReducer.world;
  // const worldExists = we(props.globalReducer.world);
  const playersByWorld = props.globalReducer.playersByWorld[props.globalReducer.world];
  const isFetching = (!playersByWorld || playersByWorld.isFetching) ? true : false;
  const loadPlayers = async (world: string) => {
    return await props.fetchPlayersIfNeeded(props.globalReducer.world);
  }
  useEffect(() => {
    let didCancel = false;
    async function getData() {
      try {
        await loadPlayers(props.globalReducer.world);
        if (!didCancel) {
          console.log('loaded?');
        }
      } catch (err) {
        console.log("error");
      }
    }
    getData();
    return () => {
      didCancel = true;
    };
  }, [props.globalReducer.world]);

  useEffect(() => {
    if (isFetching === true) {
      return;
    }
    const z = playersByWorld.players.filter((player: Player) => huntedList.includes(player.name.toLowerCase()));
    const list = huntedList.map((item: string) => {
      const isOnline = z.find((player: Player) => player.name.toLowerCase() === item.toLowerCase());
      return {
        name: item,
        online: !!isOnline,
        ...isOnline
      }
    });
    // console.log(z);
    setOnlineList(list);
  }, [huntedList, playersByWorld, isFetching]);

  useEffect(() => {
    if (isInitialMount.current) {
      isInitialMount.current = false;
      return;
   } 
    localStorage.setItem("hunted-list-temp", JSON.stringify(huntedList));
  }, [huntedList]);

  const addToList = () => {
    setHuntedList([...huntedList, ""])
  }

  const removeFromList = (index: number) => {
    const newList = huntedList.filter((item: Player, i: number) => index !== i);
    setHuntedList(newList);
  }

  const handleChange = (event: any, index: number) => {
    const { value } = event.target;
    const newHunted = huntedList.map((item: string, i: number) => index === i ? value.toLowerCase() : item);
    setHuntedList(newHunted);
  }
  const canRefresh: boolean = (playersByWorld || {}).didInvalidate || isFetching;

  const refresh = async () => {
    if (isFetching) {
      return;
    }
    await loadPlayers(props.globalReducer.world);
  }

  // if (isFetching) {
  //   return <div className="box">Loading...</div>;
  // }
  if (onlineList.length === 0 && isFetching) {
    return <div className="isFetching" style={{ marginTop: "60px" }}><Loader size="xl"/></div>;
  }

  return (
    <>
             <header className="box" style={{background:"white", padding: "15px 15px", marginBottom: "15px", marginLeft: "auto", maxWidth: "200px"}}>
             <nav className="level">
             <div className="level-item has-text-centered">
                  <div>
                <p className="heading">Players Online</p>
                <span className="title">
                  <span>{(playersByWorld || {}).players_online}</span>
                  <span className={classes({"refresh-holder": true, "can-refresh": canRefresh})} onClick={() => canRefresh && refresh()}>
                    <Tooltip html={<div><span>Updated: </span><Moment fromNow>{(playersByWorld || {}).last_updated}</Moment></div>} position="bottom">
                      <span className="icon">
                        <i className={classes({"fas fa-sync loading-paused": true, "loading-refresh": isFetching})} aria-hidden="true"></i>
                      </span>
                    </Tooltip>
                  </span>
                </span>
              </div>
              </div>
              </nav>
              </header>
    <div className="box">
      <table className="table is-bordered is-fullwidth">
  <thead>
    <tr>
      <th>Name</th>
      <th>Level</th>
      <th>Vocation</th>
      <th>Status</th>
      <th></th>
    </tr>
  </thead>
  <tbody>
      {onlineList.map((player: Player, index: number) => {
        return <tr className={`${player.online ? "is-selected" : ""}`} key={index}>
          <td><input className="input" type="text" placeholder="Name" onChange={(event) => handleChange(event, index)} value={player.name}/></td>
          <td>{player.level || "?"}</td>
          <td>{player.full_vocation || "?"}</td>
          <td>{player.online ? "ONLINE" : "OFFLINE"}</td>
          <td><button className="delete is-small" onClick={() => removeFromList(index)}></button></td>
        </tr>;
      })}
      {huntedList.length === 0 && !isFetching && <tr><td colSpan={COLSPAN}>There is noone in your hunted list.</td></tr>}
      </tbody></table>
      <aside style={{paddingTop: "24px"}}>
        <button className="button is-success is-outlined" onClick={() => addToList()}>Add new character</button>
      </aside>
    </div></>
  );
}

const mapStateToProps = state => {
  return {
    globalReducer: state.globalReducer
  };
};
const mapDispatchToProps = dispatch => ({
  fetchPlayersIfNeeded: (world) => dispatch(fetchPlayersIfNeeded(world)),
});

export default connect(mapStateToProps, mapDispatchToProps)(HuntedListView);
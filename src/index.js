import React from 'react';
import ReactDOM from 'react-dom';
// import './index.css';
import { BrowserRouter as Router, Route } from "react-router-dom";

import ErrorHandler from "./components/ErrorHandler.component";
import App from './App';
import * as serviceWorker from './serviceWorker';
//Redux:
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

import rootReducer from './reducers';

import analyticsMiddleware from "./tracking/analytics.middleware";

import LogRocket from 'logrocket';
LogRocket.init(process.env.REACT_APP_LOGROCKET_KEY);

const store = createStore(
  rootReducer,
  applyMiddleware(
    thunkMiddleware, // lets us dispatch() functions
    LogRocket.reduxMiddleware(),
    analyticsMiddleware
    // loggerMiddleware // neat middleware that logs actions
  ),
  
);

const Home = () => (
  <ErrorHandler>
  <Provider store={store}>
    <Router>
      <Route path="/" component={App} />
    </Router>
  </Provider>
  </ErrorHandler>
);

ReactDOM.render(<Home />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();

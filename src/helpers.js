import { WORLDS } from "./@globals/worlds";

export function classes(classlist: { [className: string]: boolean }): string {
  let classes: string[] = [];
  Object.keys(classlist).map(key => classlist[key] === true ? classes.push(key) : null);
  return classes.join(" ");
}

export function debounce(func: Function, wait: number, immediate: boolean): Function {
  let timeout;
  return function () {
    const context = this, args = arguments;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
};

export function getParameterByName(name: string, url: string = null): string | null {
  if (!url) url = window.location.href;
  name = name.replace(/[[\]]/g, '\\$&');
  const regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export function copyToClipboard(text: string): void {
  const dummy: HTMLElement = document.createElement("input");
  document.body.appendChild(dummy);
  dummy.setAttribute("value", text);
  dummy.select();
  document.execCommand("copy");
  document.body.removeChild(dummy);
}

export function openInNewTab(url: string): void {
  const win = window.open(url, "_blank");
  win.focus();
}

export function inArray(array: any[], needle: string | number | boolean): boolean {
  for (let i = 0; i < array.length; i += 1) {
    if (array[i] === needle) {
      return true;
    }
  }
  return false;
}

function validateValue(target: HTMLInputElement): string | number | null {
  let { value } = target;
  const { maxLength, minLength } = target;
  if (maxLength) {
    value = value.toString().slice(0, maxLength);
  }
  if (minLength) {
    //TODO:
  }
  return value;
}

export function inputChange(event: SyntheticEvent<HTMLInputElement>, cb: Function): void {
  // event.persist();
  const target = event.target;
  let value: any;
  if (target.type === "checkbox") {
    value = target.checked;
  } else if (target.type === "select-multiple") {
    value = [...target.options].filter(o => o.selected).map(o => o.value);
  } else {
    value = validateValue(target);
  }
  // const name: string = target.name;
  if (target.type === "number" && value !== "") {
    value = parseFloat(value, 10);
  }
  cb({name: target.name, value: value});
}

export function isPlayerInLevelBracket(min: number, max: number, level: number): boolean {
  if (level < min) {
    return false;
  } else if (level > max) {
    return false;
  } else {
    return true;
  }
}

export function truncate(text: string, n: number): string {
  return (text.length > n) ? text.substr(0, n-1) + String.fromCharCode(8230) : text;
}

export function worldExists(world: World | string): boolean {
  return world !== null && WORLDS[world];
}

export const getRandomArbitrary = (min: number, max: number): number => {
  return Math.random() * (max - min) + min;
};

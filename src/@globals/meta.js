import { getParameterByName } from "../helpers";

export const META_DETAILS = (pathname, searchParams) => {
  const world: World | null = getParameterByName("world", searchParams);
  const addWorldToTitle: string = world && world !== "null" ? ` for ${world}` : "";
  const meta = {
    "/share-party": () => {
      return {
        title: "Share Party" + addWorldToTitle,
        description: `An online app that lets you check LIVE Tibia character data that matches your character's share experience level criteria${addWorldToTitle}.`
      }
    },
    "default": () => {
      return {
        title: null,
        description: null
      }
    }
  };
  return (meta[pathname] || meta["default"])();
}
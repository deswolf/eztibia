// https://tibiadata.com/2018/11/tibiadata-cache-intervals/
const OVERHEAD = 500;
const OVERHEAD_AUTO = 10000;

export const CACHE_TIMES = {
  Characters: 300000,
  Worlds: 60000,
  Guilds: 120000,
  Highscore: 900000,
  Houses: 300000,
  News: 900000
};

// min * sec * ms
export const AUTO_REFRESH_TIMES = {
  Worlds: CACHE_TIMES.Worlds * 3 + OVERHEAD_AUTO, //auto refresh for getting players 
};

export const whenToInvalidate = (timestamp: string, last_updated: string, APICacheName: string): number => {
  const ms: number = CACHE_TIMES[APICacheName] - (new Date(timestamp).getTime() - new Date(last_updated).getTime()) + OVERHEAD;
  return ms > 0 ? ms : 0;
}
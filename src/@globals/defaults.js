export const DEFAULT_ME = { level: 0 };

export const BONUS_EXP_PERCENTAGE_DIFFERENT_VOCATIONS: any = {
  0: 0,
  1: 20,
  2: 30,
  3: 60,
  4: 100
};
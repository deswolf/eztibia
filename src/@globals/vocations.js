//Icons:
import Druid from "../sass/assets/Druid.gif";
import Sorcerer from "../sass/assets/Sorcerer.gif";
import Paladin from "../sass/assets/Paladin.gif";
import Knight from "../sass/assets/Knight.gif";
import Monk from "../sass/assets/Monk.gif"; // change
import None from "../sass/assets/None.gif";

export const VOCATION_NAMES_FULL = {
  Knight: "Elite Knight",
  Paladin: "Royal Paladin",
  Druid: "Elder Druid",
  Sorcerer: "Master Sorcerer",
  Monk: "Exalted Monk"
};

type VocTransformerFn<T> = (
  fullNames: { [key: string]: string },
  valueTransformer: (str: string, key: string) => T
) => { [K: string]: T };

export const vocTransformer: VocTransformerFn<mixed> = (fullNames, valueTransformer) => {
  return Object.entries(fullNames).reduce((acc, [key, value]) => ({...acc, [key]: valueTransformer(value)}) , {});
}

// export const VOCATION_NAMES = vocTransformer(VOCATION_NAMES_FULL, (_, key) => key); // { Druid: "Druid" ... }

export const VOCATIONS = vocTransformer(VOCATION_NAMES_FULL, (value) => (value.match(/\b\w/g) || []).join("").toLowerCase()); // { Druid: "ed" ... }

export const VOCATION_ICONS = {
  "Knight": Knight,
  "Paladin": Paladin,
  "Druid": Druid,
  "Sorcerer": Sorcerer,
  "Monk": Monk,
  "None": None
}
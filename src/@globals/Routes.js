export const ROUTES = (world: World) => {
  return [
    { 
      name: "Home",
      to: {
        pathname: "/",
        search: null
      }
    },
    {
      name: "Share Party",
      to: {
        pathname: "/share-party",
        search: "?" + new URLSearchParams({world: world}).toString()
      }
    },
    {
      name: "Hunted List",
      to: {
        pathname: "/hunted-list",
        search: "?" + new URLSearchParams({world: world}).toString()
      }
    },
    {
      name: "Diamond Arrows",
      to: {
        pathname: "/diamond-arrows",
        search: "?" + new URLSearchParams({world: world}).toString()
      }
    },
    {
      name: "Imbuements",
      to: {
        pathname: "/imbu",
        search: null
      }
    }
  ];
};
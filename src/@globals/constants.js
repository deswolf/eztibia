export const constants = {
  api: {
    url: process.env.REACT_APP_TIBIADATA_API,
    timeout: 10000
  }
}

export default constants;
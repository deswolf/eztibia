import { getParameterByName, isPlayerInLevelBracket } from "../helpers";
import { DEFAULT_ME } from "../@globals/defaults";
import { differenceBy } from "lodash";
import moment from "moment";
import { combineReducers } from "redux";
import LogRocket from 'logrocket';
// const UTC_OFFSET = parseInt(process.env.REACT_APP_BACKEND_UTC_OFFSET, 10);

const selectedWorld: World = getParameterByName("world") || localStorage.getItem("world") || null;

const globalState: any = {
  //world: getParameterByName("world") || localStorage.getItem("world") || null,
  isFetching: false,
  didInvalidate: false,
  players: [],
  loggedIn: 0,
  loggedOut: 0,
  players_online: 0,
  filteredPlayers: [],
  filters: [],
  me: JSON.parse(localStorage.getItem("me_" + selectedWorld)) || DEFAULT_ME,//JSON.parse(localStorage.getItem("me")) || null,
  //playersByWorld: {}
}

function world(state: World = selectedWorld, action: any) {
  switch (action.type) {
    case "SET_WORLD":
      localStorage.setItem("world", action.world);
      // localStorage.setItem("me", JSON.stringify(DEFAULT_ME)); // TODO: don't reset level
      return action.world;
    default:
      return state;
  }
};

const addVisibilityFilters = (state: any, action: any) => {
  if (action.filter.constructor !== Array) {
    action.filter = [action.filter];
  };
  // const newPlayers = applyVisibilityFilters(action.filter, state.filteredPlayers.length > 0 ? state.filteredPlayers : state.players);
  const newFilters = [
    ...state.filters,
    ...action.filter
  ];
  return {
    ...state,
    filteredPlayers: applyVisibilityFilters(action.filter, state.filteredPlayers.length > 0 ? state.filteredPlayers : state.players),
    filters: newFilters
  }
}

const applyVisibilityFilters = (filters: [] | any, players: Player[]): Player[] => {
  if (filters.constructor !== Array) {
    filters = [filters];
  };
  filters.forEach((filter) => {
    players = VisibilityFilters(filter, players);
  });
  return players;
}

const VisibilityFilters = (settings: any, players: Player[]): Player[] => {
  return {
    SHOW_ALL: () => players,
    LEVEL_FILTER: () => players.filter((player) => isPlayerInLevelBracket(settings.min, settings.max, player.level)),
    VOCATION_FILTER: () => players.filter((player) => player.vocation === settings.vocation),
    PROMOTED_FILTER: () => players.filter((player) => player.full_vocation.split(" ").length === 2)
  }[settings.type]();
}

const massagePlayers = (players: Player[]): Player => {
  return players.sort((a, b) => { return a.level - b.level }).map((element) => {
    let o: any = Object.assign({}, element);
    let vocation = o.vocation.split(" ");
    o.full_vocation = o.vocation;
    o.vocation = vocation[vocation.length - 1];
    // o.na = false; //o.vocation.split(" ").length >= 2 ? false : true
    return o;
  });
};

function players(
  state = globalState,
  action
) {
  switch (action.type) {
    case "SET_VISIBILITY_FILTERS":

    return Object.assign({}, state, {
      filteredPlayers: applyVisibilityFilters(action.filter, state.players),
      filters: action.filter
    })
    case "ADD_VISIBILITY_FILTERS":
      return addVisibilityFilters(state, action);
    case "SET_ME":
      // const me: Player | null = action.player ? Object.assign(action.player, {world: action.world}) : null;
      localStorage.setItem("me_" + action.world, JSON.stringify(action.player));
      if (action.player.name) {
        // Log rocket
        LogRocket.identify(action.player.name, {
          name: action.player.name,
          world: action.world,
          full_vocation: action.player.full_vocation,
          level: action.player.level
        });
        // TODO: Check if player's level is updated / full_vocation
      }
      return Object.assign({}, state, {
        me: action.player
      })
    case "RESET_OR_GET_ME": 
      // TODO: Read the store of all MES in different worlds
      return Object.assign({}, state, {
        me: JSON.parse(localStorage.getItem("me_" + action.world)) || DEFAULT_ME
      })
    case "INVALIDATE_PLAYERS":
      return Object.assign({}, state, {
        didInvalidate: true
      })
    case "REQUEST_PLAYERS":
      return Object.assign({}, state, {
        isFetching: true,
        didInvalidate: false
      })
    case "SET_PLAYERS":
    const players: Player[] = massagePlayers(action.players || []);
    //TODO: Maybe use from-timestamp instead of from-now: 
    const now = moment();
    const nowHour = now.hour();
    // const last_updated: moment.Date = moment(action.last_updated).utcOffset(UTC_OFFSET, true);
    const last_updated: moment.Date = moment(action.last_updated).set("hour", nowHour);
      return Object.assign({}, state, {
        isFetching: false,
        didInvalidate: false,
        loggedOut: differenceBy(state.players, players, 'name').length,
        loggedIn: differenceBy(players, state.players, 'name').length,
        players_online: action.players_online,
        players: players,
        last_updated: last_updated
      })
    default:
      return state
  }
}

function playersByWorld(state = {}, action) {
  switch (action.type) {
    case "SET_VISIBILITY_FILTERS":
    case "ADD_VISIBILITY_FILTERS":
    case "SET_ME":
    case "RESET_OR_GET_ME":
    case "INVALIDATE_PLAYERS":
    case "SET_PLAYERS":
    case "REQUEST_PLAYERS":
      return Object.assign({}, state, {
        [action.world]: players(state[action.world], action)
      })
    default:
      return state
  }
}


const rootReducer = combineReducers({
  playersByWorld,
  world
})

export default rootReducer;
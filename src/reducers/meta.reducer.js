const PAGE_NAME = process.env.REACT_APP_PAGE_NAME;

const initialMeta: any = {
  title: null,
  description: null,
  url: ""
};

function setMetaInHeader(meta: any): void {
  const headElement: HTMLElement = document.getElementsByTagName("head")[0];
  document.title = meta.title ? meta.title + " - " + PAGE_NAME : PAGE_NAME;
  meta.description && headElement.querySelector("meta[name='description']").setAttribute("content", meta.description);
  // headElement.querySelector("meta[property='og:title']").setAttribute("content", meta.title);
  // headElement.querySelector("meta[property='og:description']").setAttribute("content", meta.description);
  // headElement.querySelector("meta[property='og:url']").setAttribute("content", process.env.REACT_APP_PAGE_URL + meta.url);
}

function metaReducer(state: any = initialMeta, action: any) {
  switch (action.type) {
    case "SET_META":
      setMetaInHeader(action.meta);
      return action.meta;
    default:
      return state;
  }
};

export default metaReducer;
import { combineReducers } from "redux";
import globalReducer from "./global.reducer";
import metaReducer from "./meta.reducer";

export default combineReducers({
  globalReducer,
  metaReducer
});
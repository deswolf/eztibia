/**
 * Axios Request Wrapper
 * ---------------------
 *
 * @author  Sheharyar Naseer (@sheharyarn)
 * @license MIT
 *
 */

import axios     from 'axios';
import constants from './@globals/constants';
import { Toast } from "./components/Toast.component";


/**
 * Create an Axios Client with defaults
 */
const client = axios.create({
  baseURL: constants.api.url,
  timeout: constants.api.timeout
});



/**
 * Request Wrapper with default success/error actions
 */
const API = function(options) {
  const onSuccess = function(response) {
    // console.debug('Request Successful!', response);
    return response.data;
  }

  const onError = function(error) {
    console.error('Request Failed:', error.config);

    if (error.response) {
      // Request was made but server responded with something
      // other than 2xx
      console.error('Status:',  error.response.status);
      console.error('Data:',    error.response.data);
      console.error('Headers:', error.response.headers);

    } else {
      // Something else happened while setting up the request
      // triggered the error
      console.error('Error Message:', error.message);
    }
    Toast({ title: "Oops", body: (error.response && error.response.data && error.response.data.message) || error.message }, "error", { autoClose: 9000 })

    return Promise.reject(error.response || error.message);
  }

  return client(options)
            .then(onSuccess)
            .catch(onError);
}

export default API;
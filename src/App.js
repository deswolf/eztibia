// @flow
import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';

import SharePartyView from "./views/ShareParty.view.js";
import HeaderComponent from "./components/Header.component";
import HuntedListView from "./views/HuntedList.view";

import { ToastContainer } from 'react-toastify';
import { worldExists } from "./helpers";
import { AUTO_REFRESH_TIMES } from "./@globals/APICacheTimes";

import { toast } from 'react-toastify';
import Moment from 'react-moment';
import moment from "moment";

import { connect } from "react-redux";
import {
  fetchPlayersIfNeeded,
} from "./actions/Players.action";

import './App.scss';

import MetaSetter from "./components/MetaSetter.component";

const UTC_OFFSET = parseInt(process.env.REACT_APP_BACKEND_UTC_OFFSET, 10);

// const Home = () => <h2 style={{fontSize: "22px"}}>== Under development ==</h2>;
const NotFoundView = () => <div>Not found</div>;

type State = {
  world: World;
}

type Props = Router & {
  history: any;
  match: any;
  location: any;
  globalReducer: any;
  fetchPlayersIfNeeded: Function;
}

class App extends Component<Props, State> {


  focusWorldSelect = () => {
    this.selectWorldRef.focus();
  }

  selectWorldRef: any = React.createRef();

  get world() {
    return this.props.globalReducer.world;
  }

  get worldExists() {
    return worldExists(this.props.globalReducer.world);
  }

  get playersByWorld() {
    return this.props.globalReducer.playersByWorld[this.props.globalReducer.world] || null;
  }

  notifyLogged = (loggedIn: number, loggedOut: number, world: World, last_updated: Date): void => toast.info(
    <div>
      <h3>Number of players in {world}</h3>
        <p>logged in <span>{loggedIn}</span></p>
        <p>logged out <span>{loggedOut}</span></p>
        <p className="footer-date-notification">since <Moment interval={10} fromNow>{last_updated}</Moment></p>
    </div>, {autoClose:9000}
  );

  loadPlayers(world: World): void {
    if (this.worldExists) {
      const stateBefore: any = Object.assign({}, {first: !this.playersByWorld, didInvalidate: (this.playersByWorld || {didInvalidate: true}).didInvalidate});
      this.props.fetchPlayersIfNeeded(world).then(() => {
  
        // this.setState({timer : timer})
        if (!stateBefore.first && stateBefore.didInvalidate) {
          this.notifyLogged(this.playersByWorld.loggedIn, this.playersByWorld.loggedOut, world, this.props.globalReducer.playersByWorld[world].last_updated);
        }
        const now = moment().utcOffset(UTC_OFFSET);
        const nextUpdateAt = moment(this.playersByWorld.last_updated).utcOffset(UTC_OFFSET).add(AUTO_REFRESH_TIMES.Worlds, "ms");
        const ms = nextUpdateAt.diff(now);
        let d = moment.duration(ms).asMilliseconds();
        if (d <= 0) {
          d = AUTO_REFRESH_TIMES.Worlds;
        }
        // console.log(d, ";p");
        // clearInterval(timer);
        setTimeout(() => {
          // console.log("xf")
          this.loadPlayers(world);
        }, d);
      });
    }
  }

  componentDidMount() {
    this.loadPlayers(this.world);
    // setInterval(() => {
    //   this.loadPlayers(this.world);
    // }, AUTO_REFRESH_TIMES.Worlds);
  }

  componentWillReceiveProps(newProps: any): void {
    // if (this.props.globalReducer.world !== newProps.globalReducer.world) {
    //   this.loadPlayers(newProps.globalReducer.world);
    // }
  }


  render() {
    return (
      <Router>
        <MetaSetter/>
        <div>
          <HeaderComponent {...this.props} selectWorldRef={n => this.selectWorldRef = n} />
          <section id="main" className="hero is-link is-fullheight-with-navbar">
            <div className="container">
              <Switch>
                <Route exact path="/" render={() => <Redirect to={"/share-party?world=" + this.props.globalReducer.world}/>}/>
                <Route path="/share-party" render={(props) => <SharePartyView {...props} onFocusWorldSelect={this.focusWorldSelect} />} />
                <Route path="/hunted-list" render={(props) => <HuntedListView {...props} onFocusWorldSelect={this.focusWorldSelect} />} />
                {/* <Route path="/diamond-arrows" render={(props) => <SharePartyView {...props} onFocusWorldSelect={this.focusWorldSelect} />} /> */}
                <Route path="*" component={NotFoundView} />
              </Switch>
            </div>
          </section>
          <ToastContainer />
        </div>
      </Router>
    );
  }
}


const mapStateToProps = state => ({
  ...state
});
const mapDispatchToProps = dispatch => ({
  fetchPlayersIfNeeded: (world) => dispatch(fetchPlayersIfNeeded(world))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

// export default App;

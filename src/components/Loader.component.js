import React, { Component } from 'react';


export default class Loader extends Component {
  state = {
    hidden: this.props.isHidden || false,
    shown: this.props.isShown || true
  }
  componentWillReceiveProps({isHidden, isShown}) {
    if (isHidden !== this.props.isHidden) {
      this.setState({hidden: isHidden });
    }
    if (isShown !== this.props.isShown) {
      this.setState({shown: isShown });
    }
  }

  render() {
    const {
      props: {
        size,
        placement,
        isInline
      },
      state: {
        hidden,
        shown
      }
    } = this;
    let classes = ["l", size, placement, hidden === true ? "hidden" : null, isInline === true ? "inline" : null].filter((c) => { return c; }).join(" ");;
    // let classes = classList.filter((c) => { return c; }).join(" ");
    // console.log(classes);
    return (
      shown && <i className={classes}></i>
    );
  }
}
// @flow
import React, { Component } from 'react';
// eslint-disable-next-line
import { BrowserRouter as Router, Link} from 'react-router-dom';
// import {withRouter} from 'react-router-dom';

import Select from 'react-select';

// import Moment from 'react-moment';
import moment from 'moment';

import { Tooltip } from 'react-tippy';
import { classes } from "../helpers";

import { WORLDS } from "../@globals/worlds";
import { DAYS } from "../@globals/days";
import { RASHID } from "../@globals/rashid";
import RashidImage from "../sass/assets/Rashid.gif";

import { connect } from "react-redux";
import { setWorld } from "../actions/selectWorld.action";

import { withRouter } from "react-router";
import { ROUTES } from "../@globals/Routes";

import { getParameterByName } from "../helpers";
import JsonLd from "../components/JsonLd.component";


const PAGE_URL: string = process.env.REACT_APP_PAGE_URL || "";

const generateBreadcrumbsLd = (pathname: string, searchParams: string | null, routes: any): any => {
  let jsonLd = {
    "@context": "http://schema.org",
    "@type": "BreadcrumbList",
    "itemListElement": [
      {
        "@type": "ListItem",
        "position": 1,
        "item": {
          "@id": PAGE_URL,
          "name": "Home"
        }
      }
    ]
  };
  if (pathname === "/") {
    return jsonLd;
  }
  const route = routes.find((r) => r.to.pathname === pathname);
  if (route) {
    jsonLd.itemListElement.push(
      {
        "@type": "ListItem",
        "position": 2,
        "item": {
          "@id": PAGE_URL + pathname,
          "name": route.name
        }
      }
    );
    if (route.to.search) {
      const world: World = getParameterByName("world", searchParams);
      if (world) {
        jsonLd.itemListElement.push(
          {
            "@type": "ListItem",
            "position": 3,
            "item": {
              "@id": PAGE_URL + pathname + "?world=" + world,
              "name": world
            }
          }
        );
      }
    }
  }
  return jsonLd;
}


const WORD_LIST: any = Object.keys(WORLDS).map((key: any) => {
  return { value: WORLDS[key], label: WORLDS[key]}
});

type State = {
  //world: World;
  rashidDay: string;
  isMobileMenuOpen: boolean;
}

type Props = {
  world: World;
  worldChange: any;
  history: any;
  match: any;
  location: any;
  selectWorldRef: any;
  globalReducer: any;
  setWorld: any;
}

const navigateWithWorld = (pathname: string, world: World): any => {
  //TODO: Fix if other search params are used:
  return {
    pathname: pathname,
    search: "?" + new URLSearchParams({world: world}).toString()
  }
}

class HeaderComponent extends Component<Props, State> {
  state = {
    //world: this.props.world,// || "unknown",
    rashidDay: "",
    isMobileMenuOpen: false
  }

  get routes() {
    return ROUTES(this.props.globalReducer.world);
  }

  // selectWorldRef: any = React.createRef();

  componentWillMount() {
    this.setState({rashidDay: this.getRashid()})
  }

  updateWorldInUrl(world: World): void {
    this.props.history.push(navigateWithWorld(this.props.location.pathname, world));
  }

  getRashid(): string {
    //TODO: time diff?
    const now = moment(new Date());
    const todayServerSave = moment({hour: 9, minute: 0, seconds: 0, milliseconds: 0});
    if (now.isAfter(todayServerSave)) {
      return DAYS[moment(now).day() - 1];
    }
    return DAYS[moment(now).subtract(1, "day").day() - 1];
  }

  worldChange(event: any): void {
    const value: World = event.value;
    this.props.setWorld(value);
    this.updateWorldInUrl(value);
  }

  toggleMenuMobile(event: any): void {
    event.preventDefault();
    this.setState({isMobileMenuOpen : !this.state.isMobileMenuOpen})
  }


  render() {
    const { globalReducer } = this.props;
    const { isMobileMenuOpen } = this.state;
    return (
      <nav id="header" className="navbar">
        <div className="container">
          <div className="navbar-brand">
            <a className="navbar-item" href="/">
              {/* <img src="logo.png" width="112" height="28"/> */}
              <span style={{fontWeight:"bold", fontSize:"110%"}}>EZTibia</span>
            </a>
          </div>
          <div className={classes({"navbar-menu": true, "is-active": isMobileMenuOpen})} >
            <div className="navbar-start">
              {/* <Link className="navbar-item" to="/">Home</Link> */}
              {this.routes.map((item) => {
                if (item.name === "Home") {
                  return null;
                }
                return <Link key={item.name} className="navbar-item" to={item.to}>{item.name}</Link>;
              })}
            </div>
            <div className="navbar-end" style={{alignItems: "center"}}>
              <div><Tooltip title={RASHID[this.state.rashidDay]} position="bottom"><div className="rashid-holder"><img src={RashidImage} alt={"Rashid: " + RASHID[this.state.rashidDay]}/></div></Tooltip></div>
              <Select ref={this.props.selectWorldRef} openMenuOnFocus={true} className="worldSelect" classNamePrefix="worldSelect" value={WORD_LIST.find(o => o.value === globalReducer.world)} options={WORD_LIST} onChange={(event) => this.worldChange(event)}  />
            </div>
            <a href="#!" onClick={(e) => this.toggleMenuMobile(e)} role="button" className={classes({"navbar-burger burger": true, "is-active": isMobileMenuOpen})} aria-label="menu" aria-expanded={isMobileMenuOpen}>
              <span aria-hidden="true"></span><span aria-hidden="true"></span><span aria-hidden="true"></span>
            </a>
          </div>
        </div>
        <JsonLd data={generateBreadcrumbsLd(this.props.location.pathname, this.props.location.search, this.routes)}/>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  ...state
});
const mapDispatchToProps = dispatch => ({
  setWorld: (world) => dispatch(setWorld(world))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(HeaderComponent));
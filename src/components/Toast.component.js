import React from "react";
import { toast } from "react-toastify";

type ToastBodyProps = {
  content: { title: string; body: any }
}

const ToastBody = ({ content }: ToastBodyProps): JSX.Element => {
  return (
    <div>
      <h3>{content.title}</h3>
      <p>{content.body}</p>
    </div>
  );
};

const Toast = (
  content: { title: string; body: any },
  type: "success" | "info" | "error" | "warning" = "info",
  options: any = { autoClose: 3000 }
): any => {

  switch (type) {
    case "success": {
      toast.success(<ToastBody content={content} />, options);
      break;
    }
    case "error": {
      toast.error(<ToastBody content={content} />, options);
      break;
    }
    case "warning": {
      toast.warning(<ToastBody content={content} />, options);
      break;
    }
    case "info": {
      toast.info(<ToastBody content={content} />, options);
      break;
    }
    default: {
      break;
    }
  }
};

export { Toast };

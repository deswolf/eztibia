import React, { Component } from 'react';
// import ReactDOM from 'react-dom';


export default class ErrorHandler extends Component {
  state = {
    errorOccurred: false
  }

  componentDidCatch(error, info) {
    this.setState({ errorOccurred: true })
    // logErrorToMyService(error, info)
    // console.log(error, info);
  }

  render() {
    return this.state.errorOccurred ? <h1>Something went wrong!</h1> : this.props.children
  }
}
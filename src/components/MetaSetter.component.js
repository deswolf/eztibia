import { Component } from 'react';
import { connect } from "react-redux";
import { withRouter } from "react-router";
import { setMeta } from "../actions/Meta.action";

import { META_DETAILS } from "../@globals/meta";

class MetaSetter extends Component {

  componentWillMount(): void {
    this.props.setMeta(META_DETAILS(this.props.location.pathname, this.props.location.search));
    // console.log(this.props.location.pathname);
  }
  

  componentWillReceiveProps(newProps: any): void {
    if (this.props.location !== newProps.location) {
      this.props.setMeta(META_DETAILS(newProps.location.pathname, newProps.location.search));
    }
  }

  render() {
    return null;
  }
}

const mapStateToProps = state => ({
  ...state
});
const mapDispatchToProps = dispatch => ({
  setMeta: (meta) => dispatch(setMeta(meta))
});

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MetaSetter));
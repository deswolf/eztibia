import React, { Component } from 'react';


export default class InlineError extends Component {

  render() {
    const { title, description, icon, style, className} = this.props;
    let classes = className ? "inline-error " + className : "inline-error";
    return (
      <aside className={classes} style={style}>
        <div>
          {icon && <i className={"icon " + icon}></i>}
          {title && <h1>{title}</h1>}
          {description && <p>{description}</p>}
        </div>
      </aside>
    );
  }
}
// @flow
import React from 'react';
import { getRandomArbitrary } from "../../helpers";

export const LOADING_PLAYER_ARRAY = Array.from({ length: getRandomArbitrary(7, 16) }, (v, i) => i + 1);

const PlayerLoader = (): JSX.Element => {
  return (
    <div className="player-container">
      <div className="player-box box loading-element">
        <article className="media">
          <div className="media-content">
            <div className="content">
                <div className="player-title"><strong>.</strong></div>
                <div className="player-meta">.</div>
            </div>
          </div>
        </article>
      </div>
    </div>
  );
};

export default PlayerLoader;
import React, { Component } from 'react';
import InlineError from "../components/InlineError.component";

export default class WorldNotSelected extends Component {

  componentDidMount() {
    this.props.onFocusWorldSelect();
  }

  render() {
    return (
      <div>
        <div onClick={() => this.props.onFocusWorldSelect()} className="no-world-holder box pointer">
          <InlineError className="middle" title="Where are you from?" description="Please select your world from the list" icon="fas fa-globe-americas" />
          <i className="right-arrow-world fas fa-arrow-right"></i>
        </div>
      </div>
    );
  }
}
import React, { Component } from 'react';
import ReactDOM from 'react-dom';


export default class JsonLd extends Component {

  render() {
    return ReactDOM.createPortal(
        <script type="application/ld+json">
          {JSON.stringify(this.props.data)}
        </script>,
        document.body
      );
  }
}
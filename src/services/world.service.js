import API from "../API";

function get(name: string) {
  return API({
    url:    `world/${name}`,
    method: "GET"
  });
};

function getAll() {
  return API({
    url:    `worlds`,
    method: "GET"
  });
};

const WorldService = {
  get, getAll //create , update, delete, etc. ...
}

export default WorldService;
// import Vocation from "vocation.type";

declare type Player = {
  name: string;
  vocation: Vocation | string;
  level: number;
  // hidden: ?boolean;
  // active: ?boolean;
  na: ?boolean;
  full_vocation: Vocation | string;
  // world: ?World | string;
};

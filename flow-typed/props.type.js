declare type DefaultProps = {
  world: World;
  worldChange: any;
  history: any;
  match: any;
  location: any;
  selectWorldRef: any;
  globalReducer: any;
  setWorld: ?Function;// | any | undefined;
  onFocusWorldSelect: ?Function;// | any | undefined;
  setPlayers: ?Function;// | any | undefined;
  setVisibilityFilters: ?Function;// | any | undefined;
  setMe: ?Function;// | any | undefined;
  fetchPlayersIfNeeded: ?Function;// | any | undefined;
  invalidatePlayers: ?Function;// | any | undefined;
  setMeta: ?Function;// | any | undefined;
};
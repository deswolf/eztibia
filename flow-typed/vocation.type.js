import VOCATIONS from "../src/@globals/vocations";

// type Vocation = "knight" | "paladin" | "druid" | "sorcerer";
declare type Vocation = $Keys<typeof VOCATIONS>;
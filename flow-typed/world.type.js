import WORLDS from "../src/@globals/worlds";

declare type World = $Keys<typeof WORLDS>;
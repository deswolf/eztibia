const sm = require("sitemap");
const fs = require("fs");
// const WORLDS = require("./src/@globals/worlds.js");

//TODO: Find a way to generate dates on server based on local file
const ISO_DATES = {
  "/": "2019-04-15T18:30:12Z",
  "/share-party": "2019-12-11T21:50:00Z"
}


// const generate_urls = (url, frequency, priority, worlds) => {
//   //const lmf = `src/views/${filename}.view.js`;
//   let urls = [];
//   for (let i = 0; i < worlds.length; i += 1) { 
//     urls.push( { url: `${url}?world=${worlds[i]}`, changefreq: frequency, priority: priority, lastmodISO: ISO_DATES[url] } );
//   }
  
//   return urls;
// }
const protocolPrefix = "https://";
const PAGE_URL = process.env.PAGE_URL;

let sitemap = sm.createSitemap({
    hostname: PAGE_URL.startsWith(protocolPrefix) ? PAGE_URL : protocolPrefix + PAGE_URL,
    cacheTime: 600000,  //600 sec (10 min) cache purge period
    urls: [
        { url: "/" , changefreq: "monthly", priority: 0.9, lastmodISO: ISO_DATES["/"] },
        { url: "/share-party" , changefreq: "always", priority: 0.8, lastmodISO: ISO_DATES["/share-party"] }
        // ...generate_urls("/share-party", "always", 0.8, Object.keys(WORLDS.WORLDS))
    ]
});

fs.writeFileSync("build/sitemap.xml", sitemap.toString());